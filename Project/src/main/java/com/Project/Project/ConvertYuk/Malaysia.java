package com.Project.Project.ConvertYuk;

public class Malaysia extends Convert{
   
    private double usD, japanYen, rupiah;
    private String negaraTujuan;

    public Malaysia(String negaraTujuan, double ringgit) {
        super(ringgit);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.23;
        this.japanYen = 25.53;
        this.rupiah = 3306.34;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("jepang")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        }
    }

    public String toString() {
        return this.getInputan() + "Ringgit";
    }
}