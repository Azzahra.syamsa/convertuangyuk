package com.Project.Project.ConvertYuk;

public class USA extends Convert{

    private double rupiah, japanYen, ringgit;
    private String negaraTujuan;

    public USA(String negaraTujuan, double usD) {
        super(usD);
        this.negaraTujuan = negaraTujuan;
        this.rupiah = 14175.5;
        this.japanYen = 108.86;
        this.ringgit = 4.28;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("jepang")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        }
    }

    public String toString() {
        return this.getInputan() + "USD";
    }

}