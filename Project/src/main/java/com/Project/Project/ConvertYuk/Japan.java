package com.Project.Project.ConvertYuk;

public class Japan extends Convert{

    private double usD, rupiah, ringgit;
    private String negaraTujuan;

    public Japan(String negaraTujuan, double yen) {
        super(yen);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.0092;
        this.ringgit = 0.039;
        this.rupiah = 129.9;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        }
    }

    public String toString() {
        return this.getInputan() + "Yen";
    }

}