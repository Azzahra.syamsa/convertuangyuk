package com.Project.Project.ConvertYuk;

public class Indonesia extends Convert{

    private double usD, japanYen, ringgit;
    private String negaraTujuan;

    public Indonesia(String negaraTujuan, double rupiah) {
        super(rupiah);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.000071;
        this.japanYen = 0.0077;
        this.ringgit = 0.0003;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("jepang")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        }
    }

    public String toString() {
        return this.getInputan() + "Rupiah";
    }
    
}