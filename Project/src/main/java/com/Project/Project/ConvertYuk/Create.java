package com.Project.Project.ConvertYuk;

public class Create{

    public Create() {}

    public Convert createObject(String negaraAsal, String negaraTujuan, double inputan){
        Convert obj;
        if (negaraAsal.equalsIgnoreCase("indonesia")) {
            obj = new Indonesia(negaraTujuan, inputan);
            obj.setInputan(inputan);
            obj.converter(obj);
            System.out.println(obj.getOutputan());
        } else if (negaraAsal.equalsIgnoreCase("usa")) {
            obj = new USA(negaraTujuan, inputan);
            obj.setInputan(inputan);
            obj.converter(obj);
            System.out.println(obj.getOutputan());
        } else if (negaraAsal.equalsIgnoreCase("japan")) {
            obj = new Japan(negaraTujuan, inputan);
            obj.setInputan(inputan);
            obj.converter(obj);
            System.out.println(obj.getOutputan());
        } else if (negaraAsal.equalsIgnoreCase("malaysia")) {
            obj = new Malaysia(negaraTujuan, inputan);
            obj.setInputan(inputan);
            obj.converter(obj);
            System.out.println(obj.getOutputan());
		} else {
            obj = null;
        }
        return obj;
    }
}