package com.Project.Project.ConvertYuk;

import java.util.Scanner;

public class InputOutput{

    private static Create mataUang;
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        mataUang = new Create();
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println("=== Menu utama ===");
            System.out.println("1. Indonesia (Rupiah)");
            System.out.println("2. USA (US Dollar)");
            System.out.println("3. Jepang (Yen)");
            System.out.println("4. Malaysia (Ringgit)");
            System.out.println("5. Keluar");
            System.out.println("Masukkan nama negara asal (sesuai list):");
            String asal = input.nextLine();

            if (asal.equalsIgnoreCase("indonesia")) {
                System.out.println("Masukkan nama negara tujuan (sesuai list) :");
                String tujuan = input.nextLine();
                System.out.println("Masukkan nominal yang akan diconvert :");
                int inputan = Integer.parseInt(input.nextLine());
                mataUang.createObject("Indonesia", tujuan, inputan);
            } else if (asal.equalsIgnoreCase("usa")) {
                System.out.println("Masukkan nama negara tujuan (sesuai list) :");
                String tujuan = input.nextLine();
                System.out.println("Masukkan nominal yang akan diconvert :");
                int inputan = Integer.parseInt(input.nextLine());
                mataUang.createObject("USA", tujuan, inputan);
            } else if (asal.equalsIgnoreCase("jepang")) {
                System.out.println("Masukkan nama negara tujuan (sesuai list) :");
                String tujuan = input.nextLine();
                System.out.println("Masukkan nominal yang akan diconvert :");
                int inputan = Integer.parseInt(input.nextLine());
                mataUang.createObject("Japan", tujuan, inputan);
            } else if (asal.equalsIgnoreCase("malaysia")) {
                System.out.println("Masukkan nama negara tujuan (sesuai list) :");
                String tujuan = input.nextLine();
                System.out.println("Masukkan nominal yang akan diconvert :");
                int inputan = Integer.parseInt(input.nextLine());
                mataUang.createObject("Malaysia", tujuan, inputan);
            } else if (asal.equalsIgnoreCase("keluar")) {
                System.out.println("Terima kasih telah menggunakan Currency Converter!");
                hasChosenExit = true;
            }
        }
   }

}