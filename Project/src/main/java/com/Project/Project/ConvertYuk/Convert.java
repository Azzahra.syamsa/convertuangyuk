package com.Project.Project.ConvertYuk;

import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.text.DecimalFormat;
 
public abstract class Convert {

	private double inputan;
	private double outputan = 0.0;

	public Convert(double inputan) {
		this.inputan = inputan;
	}

	public abstract void converter(Convert asal);

	public abstract String toString();

	public void setInputan(double inputan) {
		this.inputan = inputan;
	}

	public double getInputan() {
		return inputan;
	}

	public void setOutputan(double outputan) {
		this.outputan = outputan;
	}
	
	public double getOutputan() { return outputan; }
}