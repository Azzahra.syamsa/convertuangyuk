package com.Project.Project;

import com.Project.Project.ConvertYuk.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

    @Controller
    public class MainController {

        @GetMapping("/")
        public String index(Model ConvertYuk) {
            // TODO: pass variabel yang diperlukan untuk thymeleaf di URL /
            ConvertYuk.addAttribute("nama", "Meliana");
            return "index";
        }

        @GetMapping("/form-convert")
        public String formConvert(Model ConvertYuk) {
            // TODO: pass variabel yang diperlukan untuk thymeleaf di URL /form-donasi
            ConvertYuk.addAttribute("Convert", new Create());
            return "form-convert";
        }

        @PostMapping("/form-convert")
        public String submitFormConvert(@ModelAttribute Model ConvertYuk ) {
            // TODO: tambahkan donatur yang diisi di form ke dalam daftar donatur
            ConvertYuk.addAttribute("Output", new Create());
            return "hasil-convert";
        }

    }
